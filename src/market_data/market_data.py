from binance.client import Client
import pandas as pd
import datetime as dt
import toml
import os
import numpy as np
from src.client import client
# ---------------------------------------------------------------------------- #
#                               LOAD CONFIG FILE                               #
# ---------------------------------------------------------------------------- #


config_dir = os.getcwd() + "\config\config.toml"

with open(config_dir, "r") as tomlfile:
    config = toml.load(tomlfile)

# ---------------------------------------------------------------------------- #
#                           SETUP INTERVAL DICTIONARY                          #
# ---------------------------------------------------------------------------- #

INTERVAL_DICT = {"1m": Client.KLINE_INTERVAL_1MINUTE, "5m": Client.KLINE_INTERVAL_5MINUTE,
        "15m": Client.KLINE_INTERVAL_15MINUTE, "30m": Client.KLINE_INTERVAL_30MINUTE,
        "1H": Client.KLINE_INTERVAL_1HOUR, "2H": Client.KLINE_INTERVAL_2HOUR, 
        "4H": Client.KLINE_INTERVAL_4HOUR, "12H": Client.KLINE_INTERVAL_12HOUR,
        "1D": Client.KLINE_INTERVAL_1DAY,  "1W": Client.KLINE_INTERVAL_1WEEK,
        "1M": Client.KLINE_INTERVAL_1MONTH}



class market_data:
    def __init__(self):
        self.symbol = config['BOT_SETTINGS']['symbol']
        self.symbol_list = config['BOT_SETTINGS']['symbol_list']
        self.interval = config['BOT_SETTINGS']['interval']
        self.api_key = config['API_CREDENTIALS']['api_key']
        self.api_secret = config['API_CREDENTIALS']['api_secret']
        self.client = binance_client().get_client(self.api_key, self.api_secret)

    def get_klines(self, backtesting):
        if backtesting == True:
            klines = self.client.get_historical_klines(self.symbol,
                INTERVAL_DICT[self.interval], "3650 day ago UTC")
        else:
            klines = self.client.get_historical_klines(self.symbol,
                INTERVAL_DICT[self.interval], "365 day ago UTC")

    def get_futures_klines(self, backtesting):
        if backtesting == True:
            klines = self.client.futures_historical_klines(self.symbol,
                INTERVAL_DICT[self.interval], "3650 day ago UTC")
        else:
            klines = self.client.futures_historical_klines(self.symbol,
                INTERVAL_DICT[self.interval], "365 day ago UTC")






                 


